function [point,deci] = nash_eq(A,B)
    nash_found = false;
    for col_A = 1:size(A,1)
        row_min_A = find(A(:,col_A)==min(A(:,col_A)));
        for j = 1:length(row_min_A)
            col_min_B = find(B(row_min_A(j),:)==min(B(row_min_A(j),:)));
            
            matching = find(col_A==col_min_B);
            for k = 1:matching
                nash_found = true;
                if(col_A == col_min_B(k))
                    if(row_min_A(j) == 1)
                        d1_decision = "L";
                    else
                        d1_decision = "P";
                    end

                    if(col_A == 1)
                        d2_decision = "L";
                    else
                        d2_decision = "P";
                    end
                    
                    % fprintf("Nash equilibrium found at: %c, %c\n", d1_decision,d2_decision);
                    % fprintf("Point: (%.2f , %.2f)\n",A(row_min_A(j),col_A),B(row_min_A(j),col_A));
                    point(1) = A(row_min_A(j),col_A);
                    point(2) = B(row_min_A(j),col_A);
                    deci(1) = char(d1_decision);
                    deci(2) = char(d2_decision);
    
                    % if(max(A(col_A,:))==A(row_min_A(j),col_A) & min(A(:,row_min_A(j)))==A(row_min_A(j),col_A))
                    %     if(max(B(:,col_A))==B(row_min_A(j),col_A) & min(B(row_min_A(j),:))==B(row_min_A(j),col_A))
                    %         fprintf("Minimax\n");
                    %     end
                    % end
                end
            end
        end
    end
    if(~nash_found)
        [deci(1),~]=zero_game(A);
        [~,deci(2)]=zero_game(B);
        point(1) = A(deci(1),deci(2));
        point(2) = B(deci(1),deci(2));
    end
end

