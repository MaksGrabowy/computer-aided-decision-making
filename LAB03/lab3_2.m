clear all
close all
clc
% A = [0 2 3/2 -1;
%      -3 1 1 3;
%      -1 2 1/2 2;
%       0 1 0 1];
% 
% B = [-1 1 -2/3 -1;
%        2 0 -2 1;
%        0 -1 1 -1/2;
%       -1 2 -3/2 0];

A = [0 2 3/2;
    1 1 3;
    -1 2 2];

B = [-1 1 -1;
    2 0 1;
    0 1 -1/2];

% A = [2 30;
%     0 8];
% B = [2 0;
%     30 8];

for col_A = 1:size(A,1)
    row_min_A = find(A(:,col_A)==min(A(:,col_A)));
    for j = 1:length(row_min_A)
        col_min_B = find(B(row_min_A(j),:)==min(B(row_min_A(j),:)));
        
        matching = find(col_A==col_min_B);
        for k = 1:matching
            if(col_A == col_min_B(k))
                text = sprintf("Nash equilibrium found at: %c, %c\n", char(row_min_A(j)+64),char(col_A+64));
                disp(text);
                text_2 = sprintf("Point: (%.2f , %.2f)\n",A(row_min_A(j),col_A),B(row_min_A(j),col_A));
                disp(text_2);

                if(max(A(col_A,:))==A(row_min_A(j),col_A) & min(A(:,row_min_A(j)))==A(row_min_A(j),col_A))
                    if(max(B(:,col_A))==B(row_min_A(j),col_A) & min(B(row_min_A(j),:))==B(row_min_A(j),col_A))
                        text_3 = sprintf("Minimax\n");
                        disp(text_3)
                    end
                end

            end
        end
    end
end


rational_follower = double.empty();
for row = 1:size(B,2)
    b_row_mins = find(B(row,:)==min(B(row,:)));

    mask_row = zeros(1,size(A,1));

    for i = 1:length(b_row_mins)
        a_row_mins(i) = A(row,b_row_mins(i));
        mask_row(b_row_mins(i)) = 1;
    end
    
    rational_follower(row,:) = mask_row;
    clear a_row_mins

end

res = A.*rational_follower

for row = 1:size(A,1)
    rat_row = rational_follower(row,:);
    first_choice = find(rat_row == 1);
    final_choice = first_choice(find(res(row,first_choice)==max(res(row,first_choice))));
    rat_row = zeros(1,size(A,1));
    rat_row(final_choice) = 1;
    rational_follower(row,:) = rat_row;
end

res = A.*rational_follower

for row = 1:size(A,1)
    worst(row, 2) = find(rational_follower(row,:)~=0);
    worst(row, 1) = res(row,worst(row, 2));
end
best_choice = find(worst(:,1)==min(worst(:,1)));

best_choice_leader = worst(best_choice,2);

best_choice_value = worst(best_choice,1);

if(best_choice == best_choice_leader)
    text = sprintf("Stackelberg equilibrium at %c, %c\n",char(best_choice+64),char(best_choice_leader+64));
    disp(text);
    text_2 = sprintf("Point: (%.2f , %.2f)\n",best_choice_value,B(best_choice,best_choice_leader));
    disp(text_2);
end

