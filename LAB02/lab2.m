clear all
close all
%%% MACIERZ 2x2
A = [7 0;
    -3 4];
x = (0:0.001:1);

figure();
plot(x, A(2,1)*x+A(1,1)*(1-x));
hold on;
plot(x, A(2,2)*x+A(1,2)*(1-x));
grid on;

figure();
plot(x, A(1,2)*x+A(1,1)*(1-x));
hold on;
plot(x, A(2,2)*x+A(2,1)*(1-x));
grid on


d1_list_1 = A(2,1)*x+A(1,1)*(1-x);
d2_list_1 = A(2,2)*x+A(1,2)*(1-x);
for i = 1:length(x)
    if(d1_list_1(i) > d2_list_1(i))
        envelope_upper_1(i) = d1_list_1(i);
        envelope_lower_1(i) = d2_list_1(i);
    else
        envelope_upper_1(i) = d2_list_1(i);
        envelope_lower_1(i) = d1_list_1(i);
    end
end

figure();
plot(x, envelope_upper_1);
hold on;
plot(x, envelope_lower_1);
x_min = find(envelope_upper_1==min(envelope_upper_1));
xline(x_min/length(x))
yline(envelope_lower_1(x_min))
tex = sprintf("D1 player result: %.3f at %.3f",envelope_upper_1(x_min), x_min/length(x));
disp(tex);

d1_list_2 = A(1,2)*x+A(1,1)*(1-x);
d2_list_2 = A(2,2)*x+A(2,1)*(1-x);
for i = 1:length(x)
    if(d1_list_2(i) > d2_list_2(i))
        envelope_upper_2(i) = d1_list_2(i);
        envelope_lower_2(i) = d2_list_2(i);
    else
        envelope_upper_2(i) = d2_list_2(i);
        envelope_lower_2(i) = d1_list_2(i);
    end
end

figure();
plot(x, envelope_upper_2);
hold on;
plot(x, envelope_lower_2);
x_max = find(envelope_lower_2==max(envelope_lower_2));
xline(x_max/length(x))
yline(envelope_lower_2(x_max))
tex = sprintf("D2 player result: %.3f at %.3f",envelope_lower_2(x_max), x_max/length(x));
disp(tex);


%% MACIERZ 2x3
A = [1 3 0;
    6 2 7];
[n, m] = size(A);
figure();
plot(x, A(2,1)*x+A(1,1)*(1-x));
hold on;
plot(x, A(2,2)*x+A(1,2)*(1-x));
plot(x, A(2,3)*x+A(1,3)*(1-x));

d1_list_3 = A(2,1)*x+A(1,1)*(1-x);
d2_list_3 = A(2,2)*x+A(1,2)*(1-x);
d3_list_3 = A(2,3)*x+A(1,3)*(1-x);

for i = 1:length(x)
    envelope_upper_3(i) = max([d1_list_3(i) d2_list_3(i) d3_list_3(i)]);
end

figure();
plot(x, envelope_upper_3);
grid on;
%%
figure();
plot(x, A(1,2)*x+A(1,1)*(1-x));
hold on;
plot(x, A(2,2)*x+A(2,1)*(1-x));
grid on
disp 'Wyniki dla gracza D1:'
% gracz D1
A = A';
B = ones(m, 1);
lb = zeros(n, 1);
f = (-ones(1, n));
[y,fval] = linprog(f,A,B,[],[],lb);
fval = -fval;
y = y/fval
Sm = 1/fval
disp 'Wyniki dla gracza D2:'
% gracz D2
A = A';
A = -A;
B = -ones(1, n);
lb = zeros(m, 1);
f = ones(1, m);
[z,fval] = linprog(f,A,B,[],[],lb);
z = z/fval
Sm = 1/fval