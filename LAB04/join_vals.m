function all_values = join_vals(a1,a2,a3,a4)
    all_values = cat(2,a1(1,:),a1(2,:),a2(1,:),a2(2,:),a3(1,:),a3(2,:),a4(1,:),a4(2,:));
end

