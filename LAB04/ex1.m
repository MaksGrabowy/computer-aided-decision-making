close all
clear all

a1 = [2 0;1 3];
a2 = [-1 2;0 3];
a3 = [4 1;-2 5];
a4 = [1 2;-1 0];

all_values = cat(2,a1(1,:),a1(2,:),a2(1,:),a2(2,:),a3(1,:),a3(2,:),a4(1,:),a4(2,:));

[d1_1,d2_1] = zero_game(a1);
[d1_2,d2_2] = zero_game(a2);
[d1_3,d2_3] = zero_game(a3);
[d1_4,d2_4] = zero_game(a4);


B = [a1(d1_1,d2_1) a2(d1_2,d2_2);
    a3(d1_3,d2_3) a4(d1_4,d2_4)];

decisions = [decision_description(d1_1,d2_1) decision_description(d1_2,d2_2);
    decision_description(d1_3,d2_3) decision_description(d1_4,d2_4)];

[D1,D2] = zero_game(B);

result = B(D1,D2);

res = decision_description(D1,D2);
tail = decisions(D1,D2);
disp("Ścieżka decyzji graczy")
whole_game = strcat(res,tail)
tree_result = result

draw_decision_tree(all_values,whole_game);

