close all
clear all

a1 = [0 1;2 -1];
b1 = [1 0;-1 2];

a2 = [3 4;1 5];
b2 = [1 2;1 2];

a3 = [0 2;2 2];
b3 = [-0.5 1;1 0];

a4 = [0 -2;2 -1];
b4 = [-1 1;1 0];

values_row_1 = cat(2,a1(1,:),a1(2,:),a2(1,:),a2(2,:),a3(1,:),a3(2,:),a4(1,:),a4(2,:));
values_row_2 = cat(2,b1(1,:),b1(2,:),b2(1,:),b2(2,:),b3(1,:),b3(2,:),b4(1,:),b4(2,:));

% [d1_1,d2_1] = zero_game(a1);
% [d1_2,d2_2] = zero_game(a2);
% [d1_3,d2_3] = zero_game(a3);
% [d1_4,d2_4] = zero_game(a4);

[point_1,decision_1] = nash_eq(a1,b1);
[point_2,decision_2] = nash_eq(a2,b2);
[point_3,decision_3] = nash_eq(a3,b3);
[point_4,decision_4] = nash_eq(a4,b4);


A = [point_1(1) point_2(1);
    point_3(1) point_4(1)];

B = [point_1(2) point_2(2);
    point_3(2) point_4(2)];

[point_res,decision_res] = nash_eq(A,B);
if(decision_res(1)=="P")
    D1 = 2;
else
    D1 = 1;
end
if(decision_res(2)=="P")
    D2 = 2;
else
    D2 = 1;
end

decisions = [string(decision_1) string(decision_2);
    string(decision_3) string(decision_4)];


tail = decisions(D1,D2);
disp("Ścieżka decyzji graczy")
whole_game = strcat(decision_res,tail)
tree_result = point_res

draw_decision_tree_bimatrices(values_row_1,values_row_2,whole_game);

