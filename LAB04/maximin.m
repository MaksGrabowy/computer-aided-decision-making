function [D1_strategy,D2_strategy] = maximin(A,B)
    for i=1:size(A,1)
        D1_set_min(i) = min(A(i,:));
    end

    D1_strategy = find(D1_set_min==max(D1_set_min));

    for j=1:size(B,2)
        D2_set_min(j) = min(B(:,j));
    end
    D2_strategy = find(D2_set_min==max(D2_set_min));
end

