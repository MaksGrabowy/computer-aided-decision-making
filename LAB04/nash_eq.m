function [point,deci] = nash_eq(A,B)
    nash_found = false;
    for col_A = 1:size(A,1)
        row_min_A = find(A(:,col_A)==min(A(:,col_A)));
        for j = 1:length(row_min_A)
            col_min_B = find(B(row_min_A(j),:)==min(B(row_min_A(j),:)));
            
            matching = find(col_A==col_min_B);
            for k = 1:matching
                
                if(col_A == col_min_B(k))
                    if(row_min_A(j) == 1)
                        d1_decision = "L";
                    else
                        d1_decision = "P";
                    end

                    if(col_A == 1)
                        d2_decision = "L";
                    else
                        d2_decision = "P";
                    end
                    
                    fprintf("Nash equilibrium found at: %c, %c\n", d1_decision,d2_decision);
                    fprintf("Point: (%.2f , %.2f)\n",A(row_min_A(j),col_A),B(row_min_A(j),col_A));
                    new_point(1) = A(row_min_A(j),col_A);
                    new_point(2) = B(row_min_A(j),col_A);
                    new_deci(1) = char(d1_decision);
                    new_deci(2) = char(d2_decision);
    
                    % if(max(A(col_A,:))==A(row_min_A(j),col_A) & min(A(:,row_min_A(j)))==A(row_min_A(j),col_A))
                    %     if(max(B(:,col_A))==B(row_min_A(j),col_A) & min(B(row_min_A(j),:))==B(row_min_A(j),col_A))
                    %         fprintf("Minimax\n");
                    %     end
                    % end
                    if(nash_found)
                        if(new_point(1) <= point(1) & new_point(2) <= point(2))
                            point = new_point;
                            deci = new_deci;
                        end
                    else
                        point = new_point;
                        deci = new_deci;
                    end
                    nash_found = true;
                end
            end
        end
    end
    if(~nash_found)
        [decisi_1,decisi_2]=maximin(A,B);
        % [~,decisi_2]=zero_game(B);

        point(1) = A(decisi_1,decisi_2);
        point(2) = B(decisi_1,decisi_2);

        if(decisi_1 == 1)
            deci(1) = char("L");
        else
            deci(1) = char("P");
        end

        if(decisi_2 == 1)
            deci(2) = char("L");
        else
            deci(2) = char("P");
        end
    end
end

