function draw_decision_tree_bimatrices(values_row_1,values_row_2,whole_game)
    s = [1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13 14 14 15 15];
    t = [2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31];
    weights = ones(1,length(t));
    code = {};
    for i = 1:2:30
        code{i} = 'L';
        code{i+1} = 'P';
    end
    EdgeTable = table([s' t'],weights',code','VariableNames',{'EndNodes' 'Weight' 'Code'});
    
    aliases = ["D1" "D2" "D2" "D1" "D1" "D1" "D1" "D2" "D2" "D2" "D2" "D2" "D2" "D2" "D2"];
    
    for i = 16:31
        aliases(i) = sprintf("(%.2f : %.2f)",values_row_1(i-15),values_row_2(i-15));
    end
    names = {};
    for i = 1:31
        label = sprintf('%d: %s',i,aliases(i));
        names{i} = label;
    end
    
    
    NodeTable = table(names','VariableNames',{'Name'});
    G = graph(EdgeTable,NodeTable);
    
    actual_node = 1;
    for j = 1:4
        highlight_start(j) = actual_node;
        if(whole_game{1}(j) == 'P')
           highlight_end(j) = actual_node*2+1;
           actual_node = actual_node*2+1;
        else
           highlight_end(j) = actual_node*2;
           actual_node = actual_node*2;
        end
    end
    
    H = plot(G,'EdgeLabel',G.Edges.Code);
    
    highlight(H,highlight_start,highlight_end,'EdgeColor','r','LineWidth',2)
end

