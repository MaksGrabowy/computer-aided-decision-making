function [D1_strategy,D2_strategy] = zero_game(G)
    for i=1:size(G,1)
        D1_set_min(i) = max(G(i,:));
    end
    
    for j=1:size(G,2)
        D2_set_min(j) = min(G(:,j));
    end
    
    D1_strategy = find(D1_set_min==min(D1_set_min));
    D2_strategy = find(D2_set_min==max(D2_set_min));
    
    D1_safety_min = min(D1_set_min);
    D2_safety_min = max(D2_set_min);
    
    % has_saddle_min = D1_safety_min == D2_safety_min;
end
