function result = decision_description(d1,d2)
    if(d1 == 1 & d2 == 1)
        result = "LL";
    elseif(d1 == 2 & d2 == 1)
        result = "PL";
    elseif(d1 == 2 & d2 == 2)
        result = "PP";
    elseif(d1 == 1 & d2 == 2)
        result = "LP";
    end
end

