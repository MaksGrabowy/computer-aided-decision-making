clear all
close all
% G = [ 1 3 3;
%     -2 0 -1;
%      2 1 -1;
%       2 0 0];
% G = [1 3 3 -2;
%     0 -1 2 1;
%     -2 2 0 0];

% G = [6 5 4 3 2 1;
%     1 2 3 4 5 8;
%     6 4 2 5 3 1;
%     1 5 3 2 4 7;
%     4 6 5 1 2 3;
%     3 2 1 4 5 6];

% G = [9 5 4 3 2 1;
%     1 0 3 4 5 8;
%     9 4 2 5 3 1;
%     1 5 3 2 4 7;
%     4 6 5 1 2 3;
%     3 0 1 4 5 9];

G = [6 6 4 3 2 1;
    1 6 3 4 5 8;
    6 6 2 5 3 1;
    1 6 3 2 4 7;
    4 6 5 1 2 3;
    3 6 1 4 5 6];
%% D1-min D2-max
for i=1:size(G,1)
    D1_set_min(i) = max(G(i,:));
end

for j=1:size(G,2)
    D2_set_min(j) = min(G(:,j));
end

D1_strategy_min = find(D1_set_min==min(D1_set_min));
D2_strategy_min = find(D2_set_min==max(D2_set_min));

D1_safety_min = min(D1_set_min);
D2_safety_min = max(D2_set_min);

has_saddle_min = D1_safety_min == D2_safety_min;

%% D1-max D2-min
for i=1:size(G,1)
    D1_set_max(i) = min(G(i,:));
end

for j=1:size(G,2)
    D2_set_max(j) = max(G(:,j));
end

D1_strategy_max = find(D1_set_max==max(D1_set_max));
D2_strategy_max = find(D2_set_max==min(D2_set_max));

D1_safety_max = max(D1_set_max);
D2_safety_max = min(D2_set_max);

has_saddle_max = D1_safety_max == D2_safety_max;

%% Displaying
fprintf("Player D1 strategy when minimizing: %d \n",D1_strategy_min);
fprintf("Player D1 safety level: %d \n",D1_safety_min);
fprintf("Player D2 strategy when maximizing: %d \n",D2_strategy_min);
fprintf("Player D2 safety level: %d \n",D2_safety_min);
if(has_saddle_min)
    fprintf("Game has saddle point \n\n");
else
    fprintf("Game does not have saddle point \n\n");
end

fprintf("Switch max-min\n\n")

fprintf("Player D1 strategy when maximizing: %d \n",D1_strategy_max);
fprintf("Player D1 safety level: %d \n",D1_safety_max);
fprintf("Player D2 strategy when minimizing: %d \n",D2_strategy_max);
fprintf("Player D2 safety level: %d \n",D2_safety_max);
if(has_saddle_max)
    fprintf("Game has saddle point \n\n");
else
    fprintf("Game does not have saddle point \n\n");
end